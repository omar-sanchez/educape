from functions.registrar_usuario  import RegistrarUsuario, ListarUsuarios
from functions.registrar_materiales import RegistrarMaterial, ListarMateriales
from functions.autorizar_acceso import AutorizarAcceso
from functions.mostrar_accesos_usuarios import MostrarAccesoUsuario
from functions.porcentaje_usuarios_autorizados import CalcularPorcentajeAccesos

def MenuPrincipal():
    while True:
        print("\nMenú Principal:")
        print("1. Registrar Usuario")
        print("2. Registrar Material Audiovisual")
        print("3. Listar Usuarios")
        print("4. Listar Material Audiovisual")
        print("5. Conceder Acceso al Material Audiovisual")
        print("6. Mostrar Accceso de Usuarios")
        print("7. Mostrar Porcentaje de Acceso al Material")
        print("8. Salir")

        opcion = str(input("Ingrese el número de la acción a realizar: "))
        match(opcion):
            case '1':
                RegistrarUsuario()
            case '2':
                RegistrarMaterial()
            case '3':
                ListarUsuarios()
            case '4':
                ListarMateriales()
            case '5':
                AutorizarAcceso()
            case '6':
                MostrarAccesoUsuario()
            case '7':
                CalcularPorcentajeAccesos()
            case '8':
                print("Saliendo del programa ...")
                break
            case _:
                print("La opción ingresada no es valida, ¡Intente otra vez!")

MenuPrincipal()            
        

