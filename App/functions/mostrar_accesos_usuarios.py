from functions.registrar_usuario import arrayUsuarios

def MostrarAccesoUsuario():
    correo_usuario = str(input("Ingrese el correo electrónico del usuario para ver sus accesos: "))
    usuario = next((user for user in arrayUsuarios if user.correo == correo_usuario), None)

    if(usuario):
        usuario.MostrarMaterialAccesible()
    else:
        print("Usuario no encontrado.\n")
