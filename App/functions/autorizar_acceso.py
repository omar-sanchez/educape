from functions.registrar_usuario import arrayUsuarios
from functions.registrar_materiales import arrayMateriales

def AutorizarAcceso():
    correo_usuario = str(input("Ingrese el correo electrónico del usuario para autorizar el acceso: "))
    titulo_material = str(input("Ingrese el título del material para autorizar el acceso: "))
    usuario = next((user for user in arrayUsuarios if user.correo == correo_usuario), None)
    material = next((materia for materia in arrayMateriales if materia.titulo == titulo_material), None)

    if( usuario and material):
        usuario.BrindarAccesoMaterial(material)
    else:
        print("Usuario o material no encontrado.\n")