from functions.registrar_usuario import arrayUsuarios
from functions.registrar_materiales import arrayMateriales

def CalcularPorcentajeAccesos():
    titulo_material = str(input("Ingrese el titulo del material: "))
    material = next((materia for materia in arrayMateriales if materia.titulo == titulo_material), None)

    if not material:
        print("Material no encontrado.\n")
        return
    total_usuarios = len(arrayUsuarios)
    usuarios_con_acceso = sum(material in usuario.material_con_acceso for usuario in arrayUsuarios)
    
    if total_usuarios > 0:
        porcetanje_acceso = (usuarios_con_acceso / total_usuarios) * 100
        porcetanje_sin_acceso = 100 - porcetanje_acceso
        print(f"El {porcetanje_acceso:.2f} % de los usuarios cuentan con acceso al material '{material.titulo}'.")
        print(f"El {porcetanje_sin_acceso:.2f} % de los usuarios no cuentan con acceso al material '{material.titulo}'.\n")
    else:
        print("No hay usuarios registrados para calcular el porcentaje")
