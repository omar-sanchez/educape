from classes.materia_audiovisual import MaterialAudiovisual

arrayMateriales = []

def RegistrarMaterial():
    titulo = str(input("Ingrese el título del material: "))
    modalidad = str(input("Ingrese la modalidad del material (Ejemplo, Video, Audio): "))
    nivel = str(input("Ingrese el nivel de la modalidad (Ejemplo, Principiante, Intermedio, Avanzado): "))
    area = str(input("Ingrese el área del material (Ejemplo, Matemáticas, Ciencias, Comunicación, etc): "))
    material = MaterialAudiovisual(titulo, modalidad, nivel, area)
    arrayMateriales.append(material)
    print(f"El Material: {titulo} fue registrado con éxito.\n")

def ListarMateriales():
    if not arrayMateriales:
        print("No existe materiales audiovisuales registrados.\n")
        return
    for material in arrayMateriales:
        print(f"Título: {material.titulo}, Modalidad: {material.modalidad}, Nivel: {material.nivel} Área: {material.area}")
    print()
