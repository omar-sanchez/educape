from classes.usuario import Usuario

arrayUsuarios = []

def RegistrarUsuario():
    nombres = str(input("Ingrese los nombres del usuario: "))
    apellidos = str(input("Ingrese los apellidos del usuario: "))
    dni = int(input("Ingrese el DNI del usuario: "))
    correo = str(input("Ingrese el correo electrónico del usuario: "))
    edad = int(input("Ingrese la edad del usuario: "))
    usuario = Usuario(nombres, apellidos, dni, correo, edad)
    arrayUsuarios.append(usuario)
    print(f"El usuario {nombres} fue registrado con éxito.\n")

def ListarUsuarios():
    if not arrayUsuarios:
        print("No existe usuarios registrados. \n")
        return
    for usuario in arrayUsuarios:
        print(f"Nombres: {usuario.nombres}, Apellidos: {usuario.apellidos}, DNI: {usuario.dni}, Correo: {usuario.correo}, Edad: {usuario.edad}")
    print()
