class Usuario:
    def __init__(self, nombres, apellidos, dni, correo, edad):
        self.nombres = nombres
        self.apellidos = apellidos
        self.dni = dni
        self.correo = correo
        self.edad = edad
        self.material_con_acceso = []

    def BrindarAccesoMaterial(self, material):
        self.material_con_acceso.append(material)
        print(f"Usted tiene acceso a: {material.titulo}")
    
    mensaje = ''
    response = ''
    def MostrarMaterialAccesible(self):
        if not self.material_con_acceso:
            mensaje = f"{self.nombres} no tienes acceso a ningún material audiovisual."
        else:
            mensaje = f"Lista de Materiales a los que tiene acceso el usuario: {self.nombres}"
            for material in self.material_con_acceso:
                response = f" * {material.titulo} [{material.modalidad}, {material.nivel}, {material.area}]"
        print(mensaje)
        print(response)